CREATE TABLE supplies
(
    id                   BIGSERIAL PRIMARY KEY               NOT NULL,
    pharmacy_location_id bigint                              NOT NULL,
    buy_price_per_unit   DECIMAL                             NOT NULL,
    mass                 DECIMAL                             NOT NULL,
    product_id           BIGINT                              NOT NULL,
    quantity                INT                             NOT NULL,
    expiration_date      TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    created_at           TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE TABLE alert_condition_types
(
    id          BIGSERIAL PRIMARY KEY NOT NULL,
    description VARCHAR(500)          NOT NULL
);

CREATE TABLE alert_conditions
(
    id                      BIGSERIAL PRIMARY KEY           NOT NULL,
    pharmacy_location_id    BIGINT                          NOT NULL,
    alert_condition_type_id BIGINT REFERENCES alert_condition_types (id),
    product_id              BIGINT NOT NULL,
    quantity                INT                             NOT NULL,
    notified                BOOLEAN DEFAULT FALSE NOT NULL,
    notified_date           TIMESTAMP
);

CREATE TABLE branches
(
    id      BIGSERIAL PRIMARY KEY,
    address VARCHAR(5000)
);