insert into alert_condition_types(description)
VALUES ('Stock drops to'),
       ('Monthly sales of');


-- #############################
-- TODO: DUMMY DATA
-- #############################

INSERT INTO public.branches (address)
VALUES ('Prishtine'),
       ('Mitrovice'),
       ('Ferizaj');

INSERT INTO supplies (pharmacy_location_id, buy_price_per_unit, mass, product_id, expiration_date, created_at, quantity)
VALUES (1, 0.35, 500, 8, '2024-01-01 00:00:00.000000', '2021-10-05 16:22:26.861000', 100);

INSERT INTO alert_conditions (pharmacy_location_id, alert_condition_type_id, product_id, quantity, notified, notified_date) VALUES (1, 1, 5, 10, false, null);