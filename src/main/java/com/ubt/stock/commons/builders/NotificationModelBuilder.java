package com.ubt.stock.commons.builders;

import com.ubt.stock.commons.models.NotificationDataModel;
import com.ubt.stock.commons.models.NotificationModel;

public final class NotificationModelBuilder {
  private Long id;
  private Long createdDate;
  private Long updatedDate;
  private Long userId;
  private Boolean read;
  private Boolean seen;
  private Long typeId;
  private NotificationDataModel data;

  private NotificationModelBuilder() {
  }

  public static NotificationModelBuilder aNotificationModel() {
    return new NotificationModelBuilder();
  }

  public NotificationModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public NotificationModelBuilder withCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
    return this;
  }

  public NotificationModelBuilder withUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
    return this;
  }

  public NotificationModelBuilder withUserId(Long userId) {
    this.userId = userId;
    return this;
  }

  public NotificationModelBuilder withRead(Boolean read) {
    this.read = read;
    return this;
  }

  public NotificationModelBuilder withSeen(Boolean seen) {
    this.seen = seen;
    return this;
  }

  public NotificationModelBuilder withTypeId(Long typeId) {
    this.typeId = typeId;
    return this;
  }

  public NotificationModelBuilder withData(NotificationDataModel data) {
    this.data = data;
    return this;
  }

  public NotificationModel build() {
    NotificationModel notificationModel = new NotificationModel();
    notificationModel.setId(id);
    notificationModel.setCreatedDate(createdDate);
    notificationModel.setUpdatedDate(updatedDate);
    notificationModel.setUserId(userId);
    notificationModel.setRead(read);
    notificationModel.setSeen(seen);
    notificationModel.setTypeId(typeId);
    notificationModel.setData(data);
    return notificationModel;
  }
}
