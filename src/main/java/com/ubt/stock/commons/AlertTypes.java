package com.ubt.stock.commons;

public interface AlertTypes {
  Long STOCK_DROPS_TO = 1L;
  Long WEEKLY_SALES_OF = 2L;
}
