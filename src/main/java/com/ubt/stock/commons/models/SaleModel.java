/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 29/07/2021.
 */

package com.ubt.stock.commons.models;

public class SaleModel extends BoBaseModel {
  private Long supplyId;
  private Integer quantity;
  private Long pharmacyLocationId;
  private Long productId;
  private Integer salesCount;

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public Integer getSalesCount() {
    return salesCount;
  }

  public void setSalesCount(Integer salesCount) {
    this.salesCount = salesCount;
  }

  public Long getSupplyId() {
    return supplyId;
  }

  public void setSupplyId(Long supplyId) {
    this.supplyId = supplyId;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public Long getPharmacyLocationId() {
    return pharmacyLocationId;
  }

  public void setPharmacyLocationId(Long pharmacyLocationId) {
    this.pharmacyLocationId = pharmacyLocationId;
  }
}
