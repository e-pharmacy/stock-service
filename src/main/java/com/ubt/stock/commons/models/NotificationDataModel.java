package com.ubt.stock.commons.models;

public class NotificationDataModel {
  private Long productId;
  private Integer value;

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public Integer getValue() {
    return value;
  }

  public void setValue(Integer value) {
    this.value = value;
  }
}
