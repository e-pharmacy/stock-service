package com.ubt.stock.commons.models;

import com.ubt.stock.commons.models.BoBaseModel;

public class PharmacyLocationModel extends BoBaseModel {
  private String city;

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }
}
