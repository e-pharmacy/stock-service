package com.ubt.stock.commons.models;

public class NotificationModel extends BoBaseModel {
  private Long userId;
  private Boolean read;
  private Boolean seen;
  private Long typeId;
  private NotificationDataModel data;

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Boolean getRead() {
    return read;
  }

  public void setRead(Boolean read) {
    this.read = read;
  }

  public Boolean getSeen() {
    return seen;
  }

  public void setSeen(Boolean seen) {
    this.seen = seen;
  }

  public Long getTypeId() {
    return typeId;
  }

  public void setTypeId(Long typeId) {
    this.typeId = typeId;
  }

  public NotificationDataModel getData() {
    return data;
  }

  public void setData(NotificationDataModel data) {
    this.data = data;
  }
}
