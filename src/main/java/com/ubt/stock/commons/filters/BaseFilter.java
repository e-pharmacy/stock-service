package com.ubt.stock.commons.filters;

import org.jooq.Condition;

import java.util.List;

public abstract class BaseFilter {
  private List<Long> ids;

  public List<Long> getIds() {
    return ids;
  }

  public void setIds(List<Long> ids) {
    this.ids = ids;
  }

  /**
   * Creates jooq condition based on the attributes of the filter, returns true condition if attributes are empty
   * @return org.jooq.Condition
   */
  public abstract Condition createFilterCondition();
}
