package com.ubt.stock.managesupplies.modelsvalidations;

import com.ubt.stock.exceptions.BadRequestException;
import com.ubt.stock.managesupplies.models.SupplyModel;

public class SupplyModelValidator {
  public static void isValidOnCreate(SupplyModel supplyModel) {
    isObjectEmptyForCreate(supplyModel);
  }

  public static void isValidOnUpdate(Long id, SupplyModel supplyModel) {
    if (id == null || id == 0) {
      throw new BadRequestException("Invalid product id!");
    }

    //TODO: Validate for update mode...
    //isObjectEmpty(supplyModel);
  }

  /**
   * Throws an exception if any required value of the object is not provided.
   *
   * @param supplyModel
   * @throws BadRequestException
   */
  private static void isObjectEmptyForCreate(SupplyModel supplyModel) {
    if (supplyModel.getProductId() == null || supplyModel.getProductId() == 0) {
      throw new BadRequestException("Invalid product id!");
    }

    if (supplyModel.getPharmacyLocationId() == null || supplyModel.getPharmacyLocationId() == 0) {
      throw new BadRequestException("Invalid pharmacy location!");
    }

    if (supplyModel.getBuyPricePerUnit() == null || supplyModel.getBuyPricePerUnit() == 0) {
      throw new BadRequestException("Invalid buy price per unit!");
    }

    if (supplyModel.getMass() == null || supplyModel.getMass() == 0) {
      throw new BadRequestException("Invalid product mass!");
    }
  }
}
