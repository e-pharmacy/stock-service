package com.ubt.stock.managesupplies;

import com.ubt.stock.managesupplies.filters.SupplyFilter;
import com.ubt.stock.managesupplies.models.SupplyModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/supplies")
public class ManageSuppliesController {
  private final ManageSuppliesService manageSuppliesService;

  @Autowired
  public ManageSuppliesController(ManageSuppliesService manageSuppliesService) {
    this.manageSuppliesService = manageSuppliesService;
  }

  @PostMapping("")
  public SupplyModel createSupply(@RequestBody SupplyModel supplyModel) {
    return manageSuppliesService.createSupply(supplyModel);
  }

  @GetMapping("")
  public Page<SupplyModel> getSupplies(SupplyFilter supplyFilter, Pageable pageable) {
    return manageSuppliesService.getSupplies(supplyFilter, pageable);
  }

  @GetMapping("/{id}")
  public SupplyModel getSupplyById(@PathVariable Long id) {
    return manageSuppliesService.getSupplyById(id);
  }

  @GetMapping("/search")
  public Page<SupplyModel> findSupplies(SupplyFilter supplyFilter, @RequestParam(required = false) Long pharmacyLocationId, Pageable pageable) {
    return manageSuppliesService.findSupplies(supplyFilter, pharmacyLocationId, pageable);
  }

  @PutMapping("/{id}")
  public SupplyModel updateSupply(@PathVariable Long id, @RequestBody SupplyModel supplyModel) {
    return manageSuppliesService.updateSupply(id, supplyModel);
  }

  @DeleteMapping("/{id}")
  public SupplyModel deleteSupply(@PathVariable Long id) {
    return manageSuppliesService.deleteSupply(id);
  }

  @GetMapping("/quantity")
  public Integer getProductQuantityByProductId(@RequestParam Long productId) {
    return manageSuppliesService.getProductQuantityByProductId(productId);
  }
}
