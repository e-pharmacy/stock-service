package com.ubt.stock.managesupplies;

import com.ubt.stock.alertconditions.AlertConditionsService;
import com.ubt.stock.clients.MedicineFeignClient;
import com.ubt.stock.clients.PosFeignClient;
import com.ubt.stock.commons.AlertTypes;
import com.ubt.stock.commons.exceptions.BadRequestException;
import com.ubt.stock.managebranches.ManageBranchesService;
import com.ubt.stock.managebranches.models.BranchModel;
import com.ubt.stock.managesupplies.filters.SupplyFilter;
import com.ubt.stock.managesupplies.models.ProductModel;
import com.ubt.stock.managesupplies.models.SupplyModel;
import com.ubt.stock.managesupplies.modelsvalidations.SupplyModelValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ManageSuppliesServiceImpl implements ManageSuppliesService {
  private final ManageSuppliesRepository manageSuppliesRepository;
  private final MedicineFeignClient medicineFeignClient;
  private final ManageBranchesService manageBranchesService;
  private final PosFeignClient posFeignClient;
  private final AlertConditionsService alertConditionsService;

  @Autowired
  public ManageSuppliesServiceImpl(ManageSuppliesRepository manageSuppliesRepository, MedicineFeignClient medicineFeignClient, ManageBranchesService manageBranchesService, PosFeignClient posFeignClient, AlertConditionsService alertConditionsService) {
    this.manageSuppliesRepository = manageSuppliesRepository;
    this.medicineFeignClient = medicineFeignClient;
    this.manageBranchesService = manageBranchesService;
    this.posFeignClient = posFeignClient;
    this.alertConditionsService = alertConditionsService;
  }

  @Override
  public Page<SupplyModel> getSupplies(SupplyFilter supplyFilter, Pageable pageable) {
    Page<SupplyModel> supplyModelPage = manageSuppliesRepository.getSupplies(supplyFilter, pageable);

    expandProducts(supplyModelPage.getContent());
    expandBranches(supplyModelPage.getContent());

    return supplyModelPage;
  }

  @Override
  public SupplyModel getSupplyById(Long supplyId) {
    if (supplyId == null || supplyId == 0) {
      throw new BadRequestException("Invalid Id! Please specify a valid id of the supply.");
    }

    return manageSuppliesRepository.getSupplyById(supplyId);
  }

  @Override
  public Page<SupplyModel> findSupplies(SupplyFilter supplyFilter, Long pharmacyLocationId, Pageable pageable) {
    return manageSuppliesRepository.findSupplies(supplyFilter, pharmacyLocationId, pageable);
  }

  @Override
  public SupplyModel createSupply(SupplyModel supplyModel) {
    //TODO: Branch POS location is set manually to 1. Change this after implementing this feature.
    supplyModel.setPharmacyLocationId(1L);

    SupplyModelValidator.isValidOnCreate(supplyModel);
    SupplyModel createdSupplyModel = manageSuppliesRepository.createSupply(supplyModel);

    //Set notified to false on all alerts that are with this supplied product, after the stock is resupplied with this product.
    alertConditionsService.updateAlertNotifiedStatus(createdSupplyModel.getProductId(), null, AlertTypes.STOCK_DROPS_TO, false);

    return createdSupplyModel;
  }

  @Override
  public SupplyModel updateSupply(Long id, SupplyModel supplyModel) {
    SupplyModelValidator.isValidOnUpdate(id, supplyModel);
    return manageSuppliesRepository.updateSupply(id, supplyModel);
  }

  @Override
  public SupplyModel deleteSupply(Long id) {
    canSupplyBeDeleted(id);

    return manageSuppliesRepository.deleteSupply(id);
  }

  /**
   * Counts quantity of all supplies that has same productId as the provided supply. It doesn't count quantity of provided supply.
   * Then goes to count sales based on that product. IF, sales number is higher than "all other supplies" quantity, it means that there are already some products sold for that supply and cannot be deleted!
   *
   * @param id
   */
  private void canSupplyBeDeleted(Long id) {
    Long productId = manageSuppliesRepository.getProductId(id);
    int productSuppliesCount = manageSuppliesRepository.getSuppliesQuantityCount(null, id);
    int productSalesCount = posFeignClient.getProductSalesCount(productId);
    boolean hasSupplySales = productSalesCount > productSuppliesCount;

    if (hasSupplySales) {
      throw new BadRequestException("You cannot delete this supply! There are already sold products from this supply.");
    }
  }

  private List<SupplyModel> expandProducts(List<SupplyModel> list) {
    List<Long> productsIds = list.stream().map(SupplyModel::getProductId).collect(Collectors.toList());
    List<ProductModel> productsList = medicineFeignClient.getProducts(productsIds, null, null, true);
    Map<Long, ProductModel> productsMap = productsList.stream().collect(Collectors.toMap(ProductModel::getId, e -> e));

    list.forEach(supply -> {
      ProductModel product = productsMap.get(supply.getProductId());
      supply.setProductModel(product);
    });

    return list;
  }

  private List<SupplyModel> expandBranches(List<SupplyModel> list) {
    List<Long> branchesIds = list.stream().map(SupplyModel::getPharmacyLocationId).collect(Collectors.toList());
    List<BranchModel> branchesList = manageBranchesService.getBranches(branchesIds, null);
    Map<Long, BranchModel> branchesMap = branchesList.stream().collect(Collectors.toMap(BranchModel::getId, e -> e));

    list.forEach(supply -> {
      BranchModel branch = branchesMap.get(supply.getPharmacyLocationId());
      supply.setBranchModel(branch);
    });

    return list;
  }

  @Override
  public Integer getProductQuantityByProductId(Long productId) {
    return manageSuppliesRepository.getProductQuantityByProductId(productId);
  }
}
