package com.ubt.stock.managesupplies;

import com.ubt.stock.managesupplies.filters.SupplyFilter;
import com.ubt.stock.managesupplies.models.SupplyModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ManageSuppliesService {
  Page<SupplyModel> getSupplies(SupplyFilter supplyFilter, Pageable pageable);

  SupplyModel getSupplyById(Long supplyId);

  SupplyModel createSupply(SupplyModel supplyModel);

  SupplyModel updateSupply(Long id, SupplyModel supplyModel);

  SupplyModel deleteSupply(Long id);

  Page<SupplyModel> findSupplies(SupplyFilter supplyFilter, Long pharmacyLocationId, Pageable pageable);

  Integer getProductQuantityByProductId(Long productId);
}
