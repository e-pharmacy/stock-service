package com.ubt.stock.managesupplies;

import com.ubt.stock.managesupplies.filters.SupplyFilter;
import com.ubt.stock.managesupplies.models.SupplyModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ManageSuppliesRepository {
  Page<SupplyModel> getSupplies(SupplyFilter supplyFilter, Pageable pageable);

  SupplyModel getSupplyById(Long supplyId);

  SupplyModel createSupply(SupplyModel supplyModel);

  SupplyModel updateSupply(Long id, SupplyModel supplyModel);

  SupplyModel deleteSupply(Long id);

  Page<SupplyModel> findSupplies(SupplyFilter supplyFilter, Long pharmacyLocationId, Pageable pageable);

  List<SupplyModel> getNonExpiredSuppliesByProductId(Long productId, Long pharmacyLocationId);

  Integer getProductQuantityByProductId(Long productId);

  /**
   * @param id
   * @param notInId Count all supplies quantity except this Id of supply. Also, counts only supplies that has same productId as record of supply with id notInId.
   * @return
   */
  int getSuppliesQuantityCount(Long id, Long notInId);

  Long getProductId(Long supplyId);
}
