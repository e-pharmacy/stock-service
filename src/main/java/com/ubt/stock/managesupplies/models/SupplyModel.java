package com.ubt.stock.managesupplies.models;

import com.ubt.stock.commons.models.BoBaseModel;
import com.ubt.stock.managebranches.models.BranchModel;

public class SupplyModel extends BoBaseModel {
  private Long pharmacyLocationId;
  private BranchModel branchModel;
  private Double buyPricePerUnit;
  private Double sellPricePerUnit;
  private Double mass;
  private Integer quantity;
  private Long expirationDate;
  private Long productId;
  private ProductModel productModel;

  public BranchModel getBranchModel() {
    return branchModel;
  }

  public void setBranchModel(BranchModel branchModel) {
    this.branchModel = branchModel;
  }

  public Long getPharmacyLocationId() {
    return pharmacyLocationId;
  }

  public void setPharmacyLocationId(Long pharmacyLocationId) {
    this.pharmacyLocationId = pharmacyLocationId;
  }

  public Double getBuyPricePerUnit() {
    return buyPricePerUnit;
  }

  public void setBuyPricePerUnit(Double buyPricePerUnit) {
    this.buyPricePerUnit = buyPricePerUnit;
  }

  public Double getSellPricePerUnit() {
    return sellPricePerUnit;
  }

  public void setSellPricePerUnit(Double sellPricePerUnit) {
    this.sellPricePerUnit = sellPricePerUnit;
  }

  public Double getMass() {
    return mass;
  }

  public void setMass(Double mass) {
    this.mass = mass;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public Long getExpirationDate() {
    return expirationDate;
  }

  public void setExpirationDate(Long expirationDate) {
    this.expirationDate = expirationDate;
  }

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public ProductModel getProductModel() {
    return productModel;
  }

  public void setProductModel(ProductModel productModel) {
    this.productModel = productModel;
  }
}
