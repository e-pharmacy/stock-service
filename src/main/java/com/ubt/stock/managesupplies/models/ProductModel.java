/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 29/07/2021.
 */

package com.ubt.stock.managesupplies.models;

import com.ubt.stock.commons.models.BoBaseModel;

public class ProductModel extends BoBaseModel {
  private Long drugId;
  private String imagePath;
  private Long producerCountryId;
  private String brandName;
  private String description;
  private String warning;
  private String prescription;
  private Integer barcode;
  private Long typeId;
  private Boolean recommended;
  private Double pricePerUnit;

  public Long getDrugId() {
    return drugId;
  }

  public void setDrugId(Long drugId) {
    this.drugId = drugId;
  }

  public String getImagePath() {
    return imagePath;
  }

  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  public Long getProducerCountryId() {
    return producerCountryId;
  }

  public void setProducerCountryId(Long producerCountryId) {
    this.producerCountryId = producerCountryId;
  }

  public String getBrandName() {
    return brandName;
  }

  public void setBrandName(String brandName) {
    this.brandName = brandName;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getWarning() {
    return warning;
  }

  public void setWarning(String warning) {
    this.warning = warning;
  }

  public String getPrescription() {
    return prescription;
  }

  public void setPrescription(String prescription) {
    this.prescription = prescription;
  }

  public Long getTypeId() {
    return typeId;
  }

  public void setTypeId(Long typeId) {
    this.typeId = typeId;
  }

  public Integer getBarcode() {
    return barcode;
  }

  public void setBarcode(Integer barcode) {
    this.barcode = barcode;
  }

  public Boolean getRecommended() {
    return recommended;
  }

  public void setRecommended(Boolean recommended) {
    this.recommended = recommended;
  }

  public Double getPricePerUnit() {
    return pricePerUnit;
  }

  public void setPricePerUnit(Double pricePerUnit) {
    this.pricePerUnit = pricePerUnit;
  }
}
