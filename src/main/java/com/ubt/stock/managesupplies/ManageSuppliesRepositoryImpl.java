package com.ubt.stock.managesupplies;

import com.ubt.stock.commons.pageable.PageableModel;
import com.ubt.stock.jooq.tables.records.SuppliesRecord;
import com.ubt.stock.managesupplies.filters.SupplyFilter;
import com.ubt.stock.managesupplies.jooqmappers.SupplyModelJooqMapper;
import com.ubt.stock.managesupplies.models.SupplyModel;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

import static com.ubt.stock.jooq.Tables.SUPPLIES;

@Repository
public class ManageSuppliesRepositoryImpl implements ManageSuppliesRepository {
  private final DSLContext create;

  @Autowired
  public ManageSuppliesRepositoryImpl(DSLContext create) {
    this.create = create;
  }

  @Override
  public SupplyModel createSupply(SupplyModel supplyModel) {
    SuppliesRecord suppliesRecord = SupplyModelJooqMapper.unmap(supplyModel);

    SuppliesRecord insertedSuppliesRecord = create.insertInto(SUPPLIES)
        .set(suppliesRecord)
        .returning()
        .fetchOne();

    return SupplyModelJooqMapper.map(insertedSuppliesRecord);
  }

  @Override
  public Page<SupplyModel> getSupplies(SupplyFilter supplyFilter, Pageable pageable) {
    Condition condition = supplyFilter.createFilterCondition();

    PageableModel pageableModel = new PageableModel(pageable);

    //Sort supplies by created at
    pageableModel.getOrders().add(SUPPLIES.CREATED_AT.desc());

    List<SuppliesRecord> suppliesRecords = create.selectFrom(SUPPLIES)
        .where(condition)
        .orderBy(pageableModel.getOrders())
        .offset(pageableModel.getOffset())
        .limit(pageableModel.getLimit())
        .fetch();

    int totalElements = create.selectCount().from(SUPPLIES)
        .where(condition)
        .fetchOneInto(int.class);

    List<SupplyModel> supplies = SupplyModelJooqMapper.map(suppliesRecords);

    return new PageImpl<>(supplies, pageable, totalElements);
  }

  @Override
  public SupplyModel getSupplyById(Long supplyId) {
    SuppliesRecord supplyRecord = create.selectFrom(SUPPLIES)
        .where(SUPPLIES.ID.eq(supplyId))
        .fetchOne();

    return SupplyModelJooqMapper.map(supplyRecord);
  }

  @Override
  public Page<SupplyModel> findSupplies(SupplyFilter supplyFilter, Long pharmacyLocationId, Pageable pageable) {
    //TODO: To be defined yet...

    PageableModel pageableModel = new PageableModel(pageable);
    Condition condition = supplyFilter.createFilterCondition();

//    List<SuppliesRecord> suppliesRecords = create.selectFrom(SUPPLIES)
//        .where(SUPPLIES.)
//        .and(SUPPLIES.PHARMACY_LOCATION_ID.eq(pharmacyLocationId))
//        .orderBy(pageableModel.getOrders())
//        .offset(pageableModel.getOffset())
//        .limit(pageableModel.getLimit())
//        .fetch();

//    int totalElements = create.selectCount().from(SUPPLIES)
//        .where(condition)
//        .and(SUPPLIES.PHARMACY_LOCATION_ID.eq(pharmacyLocationId))
//        .fetchOneInto(int.class);

//    return new PageImpl<>(content, pageable, total);
    return null;
  }

  @Override
  public SupplyModel updateSupply(Long id, SupplyModel supplyModel) {
    SuppliesRecord suppliesRecord = SupplyModelJooqMapper.unmap(supplyModel);

    SuppliesRecord insertedSuppliesRecord = create.update(SUPPLIES)
        .set(suppliesRecord)
        .where(SUPPLIES.ID.eq(id))
        .returning()
        .fetchOne();

    return SupplyModelJooqMapper.map(insertedSuppliesRecord);
  }

  @Override
  public SupplyModel deleteSupply(Long id) {
    SuppliesRecord insertedSuppliesRecord = create.deleteFrom(SUPPLIES)
        .where(SUPPLIES.ID.eq(id))
        .returning()
        .fetchOne();

    return SupplyModelJooqMapper.map(insertedSuppliesRecord);
  }

  @Override
  public List<SupplyModel> getNonExpiredSuppliesByProductId(Long productId, Long pharmacyLocationId) {
    List<SuppliesRecord> suppliesRecords = create.selectFrom(SUPPLIES)
        .where(SUPPLIES.PRODUCT_ID.eq(productId))
        .and(SUPPLIES.PHARMACY_LOCATION_ID.eq(pharmacyLocationId))
        .and(SUPPLIES.EXPIRATION_DATE.greaterThan(LocalDateTime.now()))
        .fetch();

    return SupplyModelJooqMapper.map(suppliesRecords);
  }

  @Override
  public Integer getProductQuantityByProductId(Long productId) {
    return create.select(DSL.sum(SUPPLIES.QUANTITY)).from(SUPPLIES)
        .where(SUPPLIES.PRODUCT_ID.eq(productId))
        .fetchOneInto(Integer.class);
  }

  @Override
  public int getSuppliesQuantityCount(Long id, Long notInId) {
    Condition condition = DSL.trueCondition();

    if (id != null) {
      condition = condition.and(SUPPLIES.ID.eq(id));
    }
    if (notInId != null) {
      condition = condition.and(SUPPLIES.ID.ne(notInId));

      //get productId of the not-in-id and get all other supplies that has the same productId as notIn supply id.s
      Long pId = getProductId(notInId);
      condition = condition.and(SUPPLIES.PRODUCT_ID.eq(pId));
    }

    return create.select(DSL.sum(SUPPLIES.QUANTITY)).from(SUPPLIES)
        .where(condition)
        .fetchOneInto(int.class);
  }

  @Override
  public Long getProductId(Long supplyId) {
    return create.select(SUPPLIES.PRODUCT_ID)
        .from(SUPPLIES)
        .where(SUPPLIES.ID.eq(supplyId))
        .fetchOneInto(Long.class);
  }
}
