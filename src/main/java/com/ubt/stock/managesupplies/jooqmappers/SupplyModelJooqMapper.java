package com.ubt.stock.managesupplies.jooqmappers;

import com.ubt.stock.commons.DateTimeService;
import com.ubt.stock.jooq.tables.records.SuppliesRecord;
import com.ubt.stock.managesupplies.builders.SupplyModelBuilder;
import com.ubt.stock.managesupplies.models.SupplyModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class SupplyModelJooqMapper {
  public static SupplyModel map(SuppliesRecord suppliesRecord) {
    return SupplyModelBuilder.aSupplyModel()
        .withId(suppliesRecord.getId())
        .withBuyPricePerUnit(suppliesRecord.getBuyPricePerUnit().doubleValue())
        .withMass(suppliesRecord.getMass().doubleValue())
        .withQuantity(suppliesRecord.getQuantity())
        .withPharmacyLocationId(suppliesRecord.getPharmacyLocationId())
        .withExpirationDate(DateTimeService.toLong(suppliesRecord.getExpirationDate()))
        .withCreatedDate(DateTimeService.toLong(suppliesRecord.getCreatedAt()))
        .withProductId(suppliesRecord.getProductId())
        .build();
  }

  public static SuppliesRecord unmap(SupplyModel supplyModel) {
    SuppliesRecord suppliesRecord = new SuppliesRecord();

    if (supplyModel.getId() != null) {
      suppliesRecord.setId(supplyModel.getId());
    }

    if (supplyModel.getProductId() != null) {
      suppliesRecord.setProductId(supplyModel.getProductId());
    }

    if (supplyModel.getBuyPricePerUnit() != null) {
      suppliesRecord.setBuyPricePerUnit(BigDecimal.valueOf(supplyModel.getBuyPricePerUnit()));
    }

    if (supplyModel.getMass() != null) {
      suppliesRecord.setMass(BigDecimal.valueOf(supplyModel.getMass()));
    }

    if (supplyModel.getQuantity() != null) {
      suppliesRecord.setQuantity(supplyModel.getQuantity());
    }

    if (supplyModel.getPharmacyLocationId() != null) {
      suppliesRecord.setPharmacyLocationId(supplyModel.getPharmacyLocationId());
    }

    if (supplyModel.getExpirationDate() != null) {
      suppliesRecord.setExpirationDate(DateTimeService.toLocalDateTime(supplyModel.getExpirationDate()));
    }

    suppliesRecord.setCreatedAt(DateTimeService.toLocalDateTime(System.currentTimeMillis()));

    return suppliesRecord;
  }

  public static List<SupplyModel> map(List<SuppliesRecord> suppliesRecords) {
    List<SupplyModel> supplies = new ArrayList<>();

    for (SuppliesRecord suppliesRecord : suppliesRecords) {
      supplies.add(map(suppliesRecord));
    }

    return supplies;
  }
}
