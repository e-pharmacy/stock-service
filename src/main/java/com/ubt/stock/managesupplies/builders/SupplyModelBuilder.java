package com.ubt.stock.managesupplies.builders;

import com.ubt.stock.managebranches.models.BranchModel;
import com.ubt.stock.managesupplies.models.ProductModel;
import com.ubt.stock.managesupplies.models.SupplyModel;

public final class SupplyModelBuilder {
  private Long id;
  private Long createdDate;
  private Long updatedDate;
  private Long pharmacyLocationId;
  private BranchModel branchModel;
  private Double buyPricePerUnit;
  private Double sellPricePerUnit;
  private Double mass;
  private Integer quantity;
  private Long expirationDate;
  private Long productId;
  private ProductModel productModel;

  private SupplyModelBuilder() {
  }

  public static SupplyModelBuilder aSupplyModel() {
    return new SupplyModelBuilder();
  }

  public SupplyModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public SupplyModelBuilder withCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
    return this;
  }

  public SupplyModelBuilder withUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
    return this;
  }

  public SupplyModelBuilder withPharmacyLocationId(Long pharmacyLocationId) {
    this.pharmacyLocationId = pharmacyLocationId;
    return this;
  }

  public SupplyModelBuilder withBranchModel(BranchModel branchModel) {
    this.branchModel = branchModel;
    return this;
  }

  public SupplyModelBuilder withBuyPricePerUnit(Double buyPricePerUnit) {
    this.buyPricePerUnit = buyPricePerUnit;
    return this;
  }

  public SupplyModelBuilder withSellPricePerUnit(Double sellPricePerUnit) {
    this.sellPricePerUnit = sellPricePerUnit;
    return this;
  }

  public SupplyModelBuilder withMass(Double mass) {
    this.mass = mass;
    return this;
  }

  public SupplyModelBuilder withQuantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }

  public SupplyModelBuilder withExpirationDate(Long expirationDate) {
    this.expirationDate = expirationDate;
    return this;
  }

  public SupplyModelBuilder withProductId(Long productId) {
    this.productId = productId;
    return this;
  }

  public SupplyModelBuilder withProductModel(ProductModel productModel) {
    this.productModel = productModel;
    return this;
  }

  public SupplyModel build() {
    SupplyModel supplyModel = new SupplyModel();
    supplyModel.setId(id);
    supplyModel.setCreatedDate(createdDate);
    supplyModel.setUpdatedDate(updatedDate);
    supplyModel.setPharmacyLocationId(pharmacyLocationId);
    supplyModel.setBranchModel(branchModel);
    supplyModel.setBuyPricePerUnit(buyPricePerUnit);
    supplyModel.setSellPricePerUnit(sellPricePerUnit);
    supplyModel.setMass(mass);
    supplyModel.setQuantity(quantity);
    supplyModel.setExpirationDate(expirationDate);
    supplyModel.setProductId(productId);
    supplyModel.setProductModel(productModel);
    return supplyModel;
  }
}
