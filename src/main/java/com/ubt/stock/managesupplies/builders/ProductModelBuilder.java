/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 29/07/2021.
 */

package com.ubt.stock.managesupplies.builders;

import com.ubt.stock.managesupplies.models.ProductModel;

public final class ProductModelBuilder {
  private Long id;
  private Long createdDate;
  private Long updatedDate;
  private Long drugId;
  private Long producerCountryId;
  private String imagePath;

  private ProductModelBuilder() {
  }

  public static ProductModelBuilder aProductModel() {
    return new ProductModelBuilder();
  }

  public ProductModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public ProductModelBuilder withCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
    return this;
  }

  public ProductModelBuilder withUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
    return this;
  }

  public ProductModelBuilder withDrugId(Long drugId) {
    this.drugId = drugId;
    return this;
  }

  public ProductModelBuilder withProducerCountryId(Long producerCountryId) {
    this.producerCountryId = producerCountryId;
    return this;
  }

  public ProductModelBuilder withImagePath(String imagePath) {
    this.imagePath = imagePath;
    return this;
  }

  public ProductModel build() {
    ProductModel productModel = new ProductModel();
    productModel.setId(id);
    productModel.setCreatedDate(createdDate);
    productModel.setUpdatedDate(updatedDate);
    productModel.setDrugId(drugId);
    productModel.setProducerCountryId(producerCountryId);
    productModel.setImagePath(imagePath);
    return productModel;
  }
}
