package com.ubt.stock.managesupplies.builders;

import com.ubt.stock.managesupplies.filters.SupplyFilter;

import java.util.List;

public final class SupplyFilterBuilder {
  private List<Long> ids;
  private Long productId;
  private List<Long> productsIds;
  private Long branchId;
  private List<Long> branchesIds;

  private SupplyFilterBuilder() {
  }

  public static SupplyFilterBuilder aSupplyFilter() {
    return new SupplyFilterBuilder();
  }

  public SupplyFilterBuilder withIds(List<Long> ids) {
    this.ids = ids;
    return this;
  }

  public SupplyFilterBuilder withProductId(Long productId) {
    this.productId = productId;
    return this;
  }

  public SupplyFilterBuilder withProductsIds(List<Long> productsIds) {
    this.productsIds = productsIds;
    return this;
  }

  public SupplyFilterBuilder withBranchId(Long branchId) {
    this.branchId = branchId;
    return this;
  }

  public SupplyFilterBuilder withBranchesIds(List<Long> branchesIds) {
    this.branchesIds = branchesIds;
    return this;
  }

  public SupplyFilter build() {
    SupplyFilter supplyFilter = new SupplyFilter();
    supplyFilter.setIds(ids);
    supplyFilter.setProductId(productId);
    supplyFilter.setProductsIds(productsIds);
    supplyFilter.setBranchId(branchId);
    supplyFilter.setBranchesIds(branchesIds);
    return supplyFilter;
  }
}
