/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 27/07/2021.
 */

package com.ubt.stock.managesupplies.filters;

import com.ubt.stock.commons.filters.BaseFilter;
import com.ubt.stock.commons.validations.ValidationService;
import org.jooq.Condition;
import org.jooq.impl.DSL;

import java.util.List;

import static com.ubt.stock.jooq.Tables.SUPPLIES;

public class SupplyFilter extends BaseFilter {
  private Long productId;
  private List<Long> productsIds;
  private Long branchId;
  private List<Long> branchesIds;

  @Override
  public Condition createFilterCondition() {
    Condition condition = DSL.trueCondition();

    if (ValidationService.notEmpty(getIds())) {
      condition = condition.and(SUPPLIES.ID.in(getIds()));
    }

    if (getProductsIds() != null) {
      condition = condition.and(SUPPLIES.PRODUCT_ID.in(getProductsIds()));
    }

    if (getBranchesIds() != null) {
      condition = condition.and(SUPPLIES.PHARMACY_LOCATION_ID.in(getBranchesIds()));
    }

    if (getProductId() != null) {
      condition = condition.and(SUPPLIES.PRODUCT_ID.eq(getProductId()));
    }

    if (getBranchId() != null) {
      condition = condition.and(SUPPLIES.PHARMACY_LOCATION_ID.eq(getBranchId()));
    }

    return condition;
  }

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public Long getBranchId() {
    return branchId;
  }

  public void setBranchId(Long branchId) {
    this.branchId = branchId;
  }

  public List<Long> getProductsIds() {
    return productsIds;
  }

  public void setProductsIds(List<Long> productsIds) {
    this.productsIds = productsIds;
  }

  public List<Long> getBranchesIds() {
    return branchesIds;
  }

  public void setBranchesIds(List<Long> branchesIds) {
    this.branchesIds = branchesIds;
  }
}
