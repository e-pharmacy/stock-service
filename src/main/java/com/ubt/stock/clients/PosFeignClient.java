package com.ubt.stock.clients;

import com.ubt.stock.commons.models.SaleModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Service
@FeignClient(value = "pos-service", url = "${feign.address.pos-service}")
public interface PosFeignClient {
  @GetMapping(value = "/sales")
  Boolean hasSupplySales(@RequestParam Long supplyId);

  @PostMapping("/sales")
  List<SaleModel> getSales(@RequestParam List<Long> productIds, @RequestParam Long branchId);

  @GetMapping("/sales")
  int getProductSalesCount(@RequestParam Long productId);
}
