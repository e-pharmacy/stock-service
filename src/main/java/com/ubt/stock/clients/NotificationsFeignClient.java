package com.ubt.stock.clients;

import com.ubt.stock.commons.models.NotificationModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Service
@FeignClient(value = "notifications-service", url = "${feign.address.notifications-service}")
public interface NotificationsFeignClient {
  @PostMapping("")
  NotificationModel add(@RequestBody NotificationModel notification);

  @PostMapping("/all")
  List<NotificationModel> addAll(@RequestBody List<NotificationModel> notifications);
}
