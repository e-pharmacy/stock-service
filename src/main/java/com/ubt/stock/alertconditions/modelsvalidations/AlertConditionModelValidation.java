/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 27/07/2021.
 */

package com.ubt.stock.alertconditions.modelsvalidations;

import com.ubt.stock.alertconditions.models.AlertConditionModel;

public class AlertConditionModelValidation {
  public static void validateOnCreate(AlertConditionModel alertConditionModel) {
    //TODO: Not fully implemented yet...
    if (alertConditionModel.getPharmacyLocationId() == null) {
      alertConditionModel.setPharmacyLocationId(0L);
    }
  }

  public static void validateOnUpdate(AlertConditionModel alertConditionModel) {
    //TODO: Not implemented yet...
  }
}
