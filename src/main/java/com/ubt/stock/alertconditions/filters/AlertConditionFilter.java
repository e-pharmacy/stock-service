/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 27/07/2021.
 */

package com.ubt.stock.alertconditions.filters;

import com.ubt.stock.commons.filters.BaseFilter;
import org.jooq.Condition;
import org.jooq.impl.DSL;

import static com.ubt.stock.jooq.Tables.ALERT_CONDITIONS;

public class AlertConditionFilter extends BaseFilter {
  private Long branchId;
  private Long productId;
  private Long typeId;

  public Long getBranchId() {
    return branchId;
  }

  public void setBranchId(Long branchId) {
    this.branchId = branchId;
  }

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public Long getTypeId() {
    return typeId;
  }

  public void setTypeId(Long typeId) {
    this.typeId = typeId;
  }

  @Override
  public Condition createFilterCondition() {
    Condition condition = DSL.trueCondition();

    if (getIds() != null && !getIds().isEmpty()) {
      condition = condition.and(ALERT_CONDITIONS.ID.in(getIds()));
    }

    if (branchId != null && branchId > 0) {
      condition = condition.and(ALERT_CONDITIONS.PHARMACY_LOCATION_ID.eq(branchId));
    }

    if (typeId != null && typeId > 0) {
      condition = condition.and(ALERT_CONDITIONS.ALERT_CONDITION_TYPE_ID.eq(typeId));
    }

    if (productId != null && productId > 0) {
      condition = condition.and(ALERT_CONDITIONS.PRODUCT_ID.eq(productId));
    }

    return condition;
  }
}
