package com.ubt.stock.alertconditions.builders;

import com.ubt.stock.alertconditions.models.AlertConditionModel;
import com.ubt.stock.alertconditions.models.AlertConditionTypeModel;
import com.ubt.stock.managebranches.models.BranchModel;
import com.ubt.stock.managesupplies.models.ProductModel;

public final class AlertConditionModelBuilder {
  private Long alertConditionTypeId;
  private Long pharmacyLocationId;
  private Long productId;
  private Integer quantity;
  private Boolean notified;
  private Long notifiedDate;
  private BranchModel pharmacyLocationModel;
  private AlertConditionTypeModel alertConditionType;
  private ProductModel product;
  private Long id;
  private Long createdDate;
  private Long updatedDate;

  private AlertConditionModelBuilder() {
  }

  public static AlertConditionModelBuilder anAlertConditionModel() {
    return new AlertConditionModelBuilder();
  }

  public AlertConditionModelBuilder withAlertConditionTypeId(Long alertConditionTypeId) {
    this.alertConditionTypeId = alertConditionTypeId;
    return this;
  }

  public AlertConditionModelBuilder withPharmacyLocationId(Long pharmacyLocationId) {
    this.pharmacyLocationId = pharmacyLocationId;
    return this;
  }

  public AlertConditionModelBuilder withProductId(Long productId) {
    this.productId = productId;
    return this;
  }

  public AlertConditionModelBuilder withQuantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }

  public AlertConditionModelBuilder withNotified(Boolean notified) {
    this.notified = notified;
    return this;
  }

  public AlertConditionModelBuilder withNotifiedDate(Long notifiedDate) {
    this.notifiedDate = notifiedDate;
    return this;
  }

  public AlertConditionModelBuilder withPharmacyLocationModel(BranchModel pharmacyLocationModel) {
    this.pharmacyLocationModel = pharmacyLocationModel;
    return this;
  }

  public AlertConditionModelBuilder withAlertConditionType(AlertConditionTypeModel alertConditionType) {
    this.alertConditionType = alertConditionType;
    return this;
  }

  public AlertConditionModelBuilder withProduct(ProductModel product) {
    this.product = product;
    return this;
  }

  public AlertConditionModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public AlertConditionModelBuilder withCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
    return this;
  }

  public AlertConditionModelBuilder withUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
    return this;
  }

  public AlertConditionModel build() {
    AlertConditionModel alertConditionModel = new AlertConditionModel();
    alertConditionModel.setAlertConditionTypeId(alertConditionTypeId);
    alertConditionModel.setPharmacyLocationId(pharmacyLocationId);
    alertConditionModel.setProductId(productId);
    alertConditionModel.setQuantity(quantity);
    alertConditionModel.setNotified(notified);
    alertConditionModel.setNotifiedDate(notifiedDate);
    alertConditionModel.setPharmacyLocationModel(pharmacyLocationModel);
    alertConditionModel.setAlertConditionType(alertConditionType);
    alertConditionModel.setProduct(product);
    alertConditionModel.setId(id);
    alertConditionModel.setCreatedDate(createdDate);
    alertConditionModel.setUpdatedDate(updatedDate);
    return alertConditionModel;
  }
}
