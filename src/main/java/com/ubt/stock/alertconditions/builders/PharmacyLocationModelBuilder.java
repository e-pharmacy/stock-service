package com.ubt.stock.alertconditions.builders;

import com.ubt.stock.commons.models.PharmacyLocationModel;

public final class PharmacyLocationModelBuilder {
  private String city;
  private Long id;
  private Long createdDate;
  private Long updatedDate;

  private PharmacyLocationModelBuilder() {
  }

  public static PharmacyLocationModelBuilder aPharmacyLocationModel() {
    return new PharmacyLocationModelBuilder();
  }

  public PharmacyLocationModelBuilder withCity(String city) {
    this.city = city;
    return this;
  }

  public PharmacyLocationModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public PharmacyLocationModelBuilder withCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
    return this;
  }

  public PharmacyLocationModelBuilder withUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
    return this;
  }

  public PharmacyLocationModel build() {
    PharmacyLocationModel pharmacyLocationModel = new PharmacyLocationModel();
    pharmacyLocationModel.setCity(city);
    pharmacyLocationModel.setId(id);
    pharmacyLocationModel.setCreatedDate(createdDate);
    pharmacyLocationModel.setUpdatedDate(updatedDate);
    return pharmacyLocationModel;
  }
}
