/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 27/07/2021.
 */

package com.ubt.stock.alertconditions.builders;

import com.ubt.stock.alertconditions.models.AlertConditionTypeModel;

public final class AlertConditionTypeModelBuilder {
  private String description;
  private Long id;
  private Long createdDate;
  private Long updatedDate;

  private AlertConditionTypeModelBuilder() {
  }

  public static AlertConditionTypeModelBuilder anAlertConditionTypeModel() {
    return new AlertConditionTypeModelBuilder();
  }

  public AlertConditionTypeModelBuilder withDescription(String description) {
    this.description = description;
    return this;
  }

  public AlertConditionTypeModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public AlertConditionTypeModelBuilder withCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
    return this;
  }

  public AlertConditionTypeModelBuilder withUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
    return this;
  }

  public AlertConditionTypeModel build() {
    AlertConditionTypeModel alertConditionTypeModel = new AlertConditionTypeModel();
    alertConditionTypeModel.setDescription(description);
    alertConditionTypeModel.setId(id);
    alertConditionTypeModel.setCreatedDate(createdDate);
    alertConditionTypeModel.setUpdatedDate(updatedDate);
    return alertConditionTypeModel;
  }
}
