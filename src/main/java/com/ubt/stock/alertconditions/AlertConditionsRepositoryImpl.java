package com.ubt.stock.alertconditions;

import com.ubt.stock.alertconditions.filters.AlertConditionFilter;
import com.ubt.stock.alertconditions.jooqmappers.AlertConditionModelJooqMapper;
import com.ubt.stock.alertconditions.jooqmappers.AlertConditionTypeModelJooqMapper;
import com.ubt.stock.alertconditions.models.AlertConditionModel;
import com.ubt.stock.alertconditions.models.AlertConditionTypeModel;
import com.ubt.stock.jooq.tables.records.AlertConditionTypesRecord;
import com.ubt.stock.jooq.tables.records.AlertConditionsRecord;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

import static com.ubt.stock.jooq.Tables.ALERT_CONDITIONS;
import static com.ubt.stock.jooq.Tables.ALERT_CONDITION_TYPES;

@Repository
public class AlertConditionsRepositoryImpl implements AlertConditionsRepository {
  private final DSLContext create;

  @Autowired
  public AlertConditionsRepositoryImpl(DSLContext create) {
    this.create = create;
  }

  @Override
  public List<AlertConditionTypeModel> getAlertConditionTypes(List<Long> ids, String query) {
    Condition condition = DSL.trueCondition();

    if (ids != null && !ids.isEmpty()) {
      condition = ALERT_CONDITION_TYPES.ID.in(ids);
    }
    if (query != null && !query.trim().isEmpty()) {
      condition = ALERT_CONDITION_TYPES.DESCRIPTION.containsIgnoreCase(query);
    }

    List<AlertConditionTypesRecord> alertConditionTypesRecordList = create.selectFrom(ALERT_CONDITION_TYPES)
        .where(condition)
        .fetch();

    return AlertConditionTypeModelJooqMapper.map(alertConditionTypesRecordList);
  }

  @Override
  public AlertConditionModel createAlertCondition(AlertConditionModel alertConditionModel) {
    AlertConditionsRecord alertConditionsRecord = AlertConditionModelJooqMapper.unmap(alertConditionModel);

    AlertConditionsRecord insertedAlertConditionsRecord = create.insertInto(ALERT_CONDITIONS)
        .set(alertConditionsRecord)
        .returning()
        .fetchOne();

    return AlertConditionModelJooqMapper.map(insertedAlertConditionsRecord);
  }

  @Override
  public List<AlertConditionModel> getAlertConditions(AlertConditionFilter alertConditionFilter) {
    Condition condition = alertConditionFilter.createFilterCondition();

    List<AlertConditionsRecord> alertConditionsRecords = create.selectFrom(ALERT_CONDITIONS)
        .where(condition)
        .fetch();

    return AlertConditionModelJooqMapper.map(alertConditionsRecords);
  }

  @Override
  public AlertConditionModel getAlertConditionById(Long id) {
    AlertConditionsRecord alertConditionTypesRecord = create.selectFrom(ALERT_CONDITIONS)
        .where(ALERT_CONDITIONS.ID.eq(id))
        .fetchOne();

    return AlertConditionModelJooqMapper.map(alertConditionTypesRecord);
  }

  @Override
  public AlertConditionModel updateAlertConditions(Long id, AlertConditionModel alertConditionModel, Long pharmacyLocationId) {
    AlertConditionsRecord alertConditionsRecord = AlertConditionModelJooqMapper.unmap(alertConditionModel);

    AlertConditionsRecord updatedAlertConditionsRecord = create.update(ALERT_CONDITIONS)
        .set(alertConditionsRecord)
        .where(ALERT_CONDITIONS.ID.eq(id))
        .returning()
        .fetchOne();

    return AlertConditionModelJooqMapper.map(updatedAlertConditionsRecord);
  }

  @Override
  public AlertConditionModel deleteAlertCondition(Long id) {
    AlertConditionsRecord deletedAlertConditionsRecord = create.deleteFrom(ALERT_CONDITIONS)
        .where(ALERT_CONDITIONS.ID.eq(id))
        .returning()
        .fetchOne();

    return AlertConditionModelJooqMapper.map(deletedAlertConditionsRecord);
  }

  @Override
  public void updateAlertNotifiedStatus(Long productId, Long alertId, Long typeId, Boolean status) {
    Condition condition = DSL.trueCondition();

    if (productId != null) {
      condition = condition.and(ALERT_CONDITIONS.PRODUCT_ID.eq(productId));
    }

    if (alertId != null) {
      condition = condition.and(ALERT_CONDITIONS.ID.eq(productId));
    }

    if (typeId != null) {
      condition = condition.and(ALERT_CONDITIONS.ALERT_CONDITION_TYPE_ID.eq(typeId));
    }

    create.update(ALERT_CONDITIONS)
        .set(ALERT_CONDITIONS.NOTIFIED, status)
        .set(ALERT_CONDITIONS.NOTIFIED_DATE, Boolean.TRUE.equals(status) ? LocalDateTime.now() : null)
        .where(condition)
        .execute();
  }
}
