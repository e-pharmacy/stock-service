package com.ubt.stock.alertconditions;

import com.ubt.stock.alertconditions.filters.AlertConditionFilter;
import com.ubt.stock.alertconditions.models.AlertConditionModel;
import com.ubt.stock.alertconditions.models.AlertConditionTypeModel;

import java.util.List;

public interface AlertConditionsRepository {
  List<AlertConditionTypeModel> getAlertConditionTypes(List<Long> ids, String query);

  AlertConditionModel createAlertCondition(AlertConditionModel alertConditionModel);

  List<AlertConditionModel> getAlertConditions(AlertConditionFilter alertConditionFilter);

  AlertConditionModel getAlertConditionById(Long id);

  AlertConditionModel updateAlertConditions(Long id, AlertConditionModel alertConditionModel, Long pharmacyLocationId);

  AlertConditionModel deleteAlertCondition(Long id);

  void updateAlertNotifiedStatus(Long productId, Long alertId, Long typeId, Boolean status);
}
