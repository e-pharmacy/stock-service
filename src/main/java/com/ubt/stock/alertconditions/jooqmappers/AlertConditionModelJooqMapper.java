/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 27/07/2021.
 */

package com.ubt.stock.alertconditions.jooqmappers;

import com.ubt.stock.alertconditions.builders.AlertConditionModelBuilder;
import com.ubt.stock.alertconditions.models.AlertConditionModel;
import com.ubt.stock.commons.DateTimeService;
import com.ubt.stock.jooq.tables.records.AlertConditionsRecord;

import java.util.ArrayList;
import java.util.List;

public class AlertConditionModelJooqMapper {
  public static AlertConditionModel map(AlertConditionsRecord alertConditionsRecord) {
    return AlertConditionModelBuilder.anAlertConditionModel()
        .withId(alertConditionsRecord.getId())
        .withAlertConditionTypeId(alertConditionsRecord.getAlertConditionTypeId())
        .withProductId(alertConditionsRecord.getProductId())
        .withQuantity(alertConditionsRecord.getQuantity())
        .withPharmacyLocationId(alertConditionsRecord.getPharmacyLocationId())
        .withNotified(alertConditionsRecord.getNotified())
        .withNotifiedDate(DateTimeService.toLong(alertConditionsRecord.getNotifiedDate()))
        .build();
  }

  public static AlertConditionsRecord unmap(AlertConditionModel alertConditionModel) {
    AlertConditionsRecord alertConditionsRecord = new AlertConditionsRecord();

    if (alertConditionModel.getId() != null) {
      alertConditionsRecord.setId(alertConditionModel.getId());
    }

    if (alertConditionModel.getAlertConditionTypeId() != null) {
      alertConditionsRecord.setAlertConditionTypeId(alertConditionModel.getAlertConditionTypeId());
    }

    if (alertConditionModel.getProductId() != null) {
      alertConditionsRecord.setProductId(alertConditionModel.getProductId());
    }

    if (alertConditionModel.getQuantity() != null) {
      alertConditionsRecord.setQuantity(alertConditionModel.getQuantity());
    }

    if (alertConditionModel.getPharmacyLocationId() != null) {
      alertConditionsRecord.setPharmacyLocationId(alertConditionModel.getPharmacyLocationId());
    }

    if (alertConditionModel.getNotified() != null) {
      alertConditionsRecord.setNotified(alertConditionModel.getNotified());
    }

    if (alertConditionModel.getNotifiedDate() != null) {
      alertConditionsRecord.setNotifiedDate(DateTimeService.toLocalDateTime(alertConditionModel.getNotifiedDate()));
    }

    return alertConditionsRecord;
  }

  public static List<AlertConditionModel> map(List<AlertConditionsRecord> alertConditionsRecords) {
    List<AlertConditionModel> alerts = new ArrayList<>();

    for (AlertConditionsRecord alertConditionsRecord : alertConditionsRecords) {
      alerts.add(map(alertConditionsRecord));
    }

    return alerts;
  }
}
