/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 27/07/2021.
 */

package com.ubt.stock.alertconditions.jooqmappers;

import com.ubt.stock.alertconditions.builders.AlertConditionTypeModelBuilder;
import com.ubt.stock.alertconditions.models.AlertConditionTypeModel;
import com.ubt.stock.jooq.tables.records.AlertConditionTypesRecord;

import java.util.ArrayList;
import java.util.List;

public class AlertConditionTypeModelJooqMapper {
  public static AlertConditionTypeModel map(AlertConditionTypesRecord alertConditionTypesRecord) {
    return AlertConditionTypeModelBuilder.anAlertConditionTypeModel()
        .withId(alertConditionTypesRecord.getId())
        .withDescription(alertConditionTypesRecord.getDescription())
        .build();
  }

  public static List<AlertConditionTypeModel> map(List<AlertConditionTypesRecord> alertConditionTypesRecordList) {
    List<AlertConditionTypeModel> alertConditionTypeModels = new ArrayList<>();

    for (AlertConditionTypesRecord alertConditionTypesRecord : alertConditionTypesRecordList) {
      alertConditionTypeModels.add(map(alertConditionTypesRecord));
    }

    return alertConditionTypeModels;
  }
}
