package com.ubt.stock.alertconditions.models;

import com.ubt.stock.commons.models.BoBaseModel;

public class AlertConditionTypeModel extends BoBaseModel {
  private String description;

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
