package com.ubt.stock.alertconditions.models;

import com.ubt.stock.commons.models.BoBaseModel;
import com.ubt.stock.commons.models.PharmacyLocationModel;
import com.ubt.stock.managebranches.models.BranchModel;
import com.ubt.stock.managesupplies.models.ProductModel;

public class AlertConditionModel extends BoBaseModel {
  private Long alertConditionTypeId;
  private Long pharmacyLocationId;
  private Long productId;
  private Integer quantity;
  private Boolean notified;
  private Long notifiedDate;
  private BranchModel pharmacyLocationModel;
  private AlertConditionTypeModel alertConditionType;
  private ProductModel product;

  public ProductModel getProduct() {
    return product;
  }

  public void setProduct(ProductModel productModel) {
    this.product = productModel;
  }

  public Long getPharmacyLocationId() {
    return pharmacyLocationId;
  }

  public void setPharmacyLocationId(Long pharmacyLocationId) {
    this.pharmacyLocationId = pharmacyLocationId;
  }

  public BranchModel getPharmacyLocationModel() {
    return pharmacyLocationModel;
  }

  public void setPharmacyLocationModel(BranchModel pharmacyLocationModel) {
    this.pharmacyLocationModel = pharmacyLocationModel;
  }

  public AlertConditionTypeModel getAlertConditionType() {
    return alertConditionType;
  }

  public void setAlertConditionType(AlertConditionTypeModel alertConditionType) {
    this.alertConditionType = alertConditionType;
  }

  public Long getAlertConditionTypeId() {
    return alertConditionTypeId;
  }

  public void setAlertConditionTypeId(Long alertConditionTypeId) {
    this.alertConditionTypeId = alertConditionTypeId;
  }

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public Boolean getNotified() {
    return notified;
  }

  public void setNotified(Boolean notified) {
    this.notified = notified;
  }

  public Long getNotifiedDate() {
    return notifiedDate;
  }

  public void setNotifiedDate(Long notifiedDate) {
    this.notifiedDate = notifiedDate;
  }
}
