package com.ubt.stock.alertconditions;

import com.ubt.stock.alertconditions.filters.AlertConditionFilter;
import com.ubt.stock.alertconditions.models.AlertConditionModel;
import com.ubt.stock.alertconditions.models.AlertConditionTypeModel;
import com.ubt.stock.alertconditions.modelsvalidations.AlertConditionModelValidation;
import com.ubt.stock.clients.MedicineFeignClient;
import com.ubt.stock.commons.AlertTypes;
import com.ubt.stock.commons.models.BoBaseModel;
import com.ubt.stock.managebranches.ManageBranchesService;
import com.ubt.stock.managebranches.models.BranchModel;
import com.ubt.stock.managesupplies.models.ProductModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AlertConditionsServiceImpl implements AlertConditionsService {
  private final AlertConditionsRepository alertConditionsRepository;
  private final MedicineFeignClient medicineFeignClient;
  private final ManageBranchesService manageBranchesService;

  @Autowired
  public AlertConditionsServiceImpl(AlertConditionsRepository alertConditionsRepository, MedicineFeignClient medicineFeignClient, ManageBranchesService manageBranchesService) {
    this.alertConditionsRepository = alertConditionsRepository;
    this.medicineFeignClient = medicineFeignClient;
    this.manageBranchesService = manageBranchesService;
  }

  @Override
  public List<AlertConditionTypeModel> getAlertConditionTypes(List<Long> ids, String query) {
    return alertConditionsRepository.getAlertConditionTypes(ids, query);
  }

  @Override
  public AlertConditionModel createAlertCondition(AlertConditionModel alertConditionModel) {
    AlertConditionModelValidation.validateOnCreate(alertConditionModel);
    return alertConditionsRepository.createAlertCondition(alertConditionModel);
  }

  @Override
  public List<AlertConditionModel> getAlertConditions(AlertConditionFilter alertConditionFilter) {
    List<AlertConditionModel> alertConditionModelList = alertConditionsRepository.getAlertConditions(alertConditionFilter);

    List<Long> productIds = new ArrayList<>();
    List<Long> alertConditionTypeIds = new ArrayList<>();
    List<Long> pharmacyLocationIds = new ArrayList<>();

    alertConditionModelList.forEach(e -> {
      productIds.add(e.getProductId());
      alertConditionTypeIds.add(e.getAlertConditionTypeId());
      pharmacyLocationIds.add(e.getPharmacyLocationId());
    });

    Map<Long, ProductModel> productsMap = convertProductListToMap(medicineFeignClient.getProducts(productIds, null, null, true));
    Map<Long, AlertConditionTypeModel> alertConditionTypesMap = convertConditionTypesListToMap(getAlertConditionTypes(alertConditionTypeIds, null));
    Map<Long, BranchModel> pharmacyLocationsMap = convertBranchesListToMap(manageBranchesService.getBranches(pharmacyLocationIds, null));

    alertConditionModelList.forEach(e -> {
      e.setProduct(productsMap.get(e.getProductId()));
      e.setAlertConditionType(alertConditionTypesMap.get(e.getAlertConditionTypeId()));
      e.setPharmacyLocationModel(pharmacyLocationsMap.get(e.getPharmacyLocationId()));
    });

    return alertConditionModelList;
  }

  @Override
  public AlertConditionModel getAlertConditionById(Long id) {
    return alertConditionsRepository.getAlertConditionById(id);
  }

  @Override
  public AlertConditionModel updateAlertConditions(Long id, AlertConditionModel alertConditionModel, Long pharmacyLocationId) {
    AlertConditionModelValidation.validateOnUpdate(alertConditionModel);
    return alertConditionsRepository.updateAlertConditions(id, alertConditionModel, pharmacyLocationId);
  }

  @Override
  public AlertConditionModel deleteAlertCondition(Long id) {
    return alertConditionsRepository.deleteAlertCondition(id);
  }

  @Override
  public void updateAlertNotifiedStatus(Long productId, Long alertId, Long typeId, Boolean status) {
    alertConditionsRepository.updateAlertNotifiedStatus(productId, alertId, typeId, status);
  }

  private Map<Long, ProductModel> convertProductListToMap(List<ProductModel> products) {
    return products.stream().collect(Collectors.toMap(BoBaseModel::getId, e -> e));
  }

  private Map<Long, BranchModel> convertBranchesListToMap(List<BranchModel> branches) {
    return branches.stream().collect(Collectors.toMap(BoBaseModel::getId, e -> e));
  }

  private Map<Long, AlertConditionTypeModel> convertConditionTypesListToMap(List<AlertConditionTypeModel> alertConditionTypes) {
    return alertConditionTypes.stream().collect(Collectors.toMap(BoBaseModel::getId, e -> e));
  }
}
