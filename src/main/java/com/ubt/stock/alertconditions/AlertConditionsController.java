package com.ubt.stock.alertconditions;

import com.ubt.stock.alertconditions.filters.AlertConditionFilter;
import com.ubt.stock.alertconditions.models.AlertConditionModel;
import com.ubt.stock.alertconditions.models.AlertConditionTypeModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/alerts/conditions")
public class AlertConditionsController {
  private final AlertConditionsService alertConditionsService;

  @Autowired
  public AlertConditionsController(AlertConditionsService alertConditionsService) {
    this.alertConditionsService = alertConditionsService;
  }

  @GetMapping("/types")
  public List<AlertConditionTypeModel> getAlertConditionTypes(@RequestParam(required = false) List<Long> ids, @RequestParam(required = false) String query) {
    return alertConditionsService.getAlertConditionTypes(ids, query);
  }

  @PostMapping("")
  public AlertConditionModel createAlertCondition(@RequestBody AlertConditionModel alertConditionModel) {
    return alertConditionsService.createAlertCondition(alertConditionModel);
  }

  @GetMapping("")
  public List<AlertConditionModel> getAlertConditions(AlertConditionFilter alertConditionFilter) {
    return alertConditionsService.getAlertConditions(alertConditionFilter);
  }

  @GetMapping("/{id}")
  public AlertConditionModel getAlertConditionById(@PathVariable Long id) {
    return alertConditionsService.getAlertConditionById(id);
  }

  @PutMapping("/{id}")
  public AlertConditionModel updateAlertConditions(@PathVariable Long id, @RequestBody AlertConditionModel alertConditionModel, @RequestParam(required = false) Long pharmacyLocationId) {
    return alertConditionsService.updateAlertConditions(id, alertConditionModel, pharmacyLocationId);
  }

  @DeleteMapping("/{id}")
  public AlertConditionModel deleteAlertCondition(@PathVariable Long id) {
    return alertConditionsService.deleteAlertCondition(id);
  }
}
