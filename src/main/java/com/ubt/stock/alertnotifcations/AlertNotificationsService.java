package com.ubt.stock.alertnotifcations;

public interface AlertNotificationsService {
  void processAlerts();
}
