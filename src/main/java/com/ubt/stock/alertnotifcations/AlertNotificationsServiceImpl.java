package com.ubt.stock.alertnotifcations;

import com.ubt.stock.alertconditions.AlertConditionsService;
import com.ubt.stock.alertconditions.models.AlertConditionModel;
import com.ubt.stock.clients.NotificationsFeignClient;
import com.ubt.stock.clients.PosFeignClient;
import com.ubt.stock.commons.AlertTypes;
import com.ubt.stock.commons.builders.NotificationModelBuilder;
import com.ubt.stock.commons.models.NotificationDataModel;
import com.ubt.stock.commons.models.NotificationModel;
import com.ubt.stock.commons.models.SaleModel;
import com.ubt.stock.managesupplies.ManageSuppliesService;
import com.ubt.stock.managesupplies.builders.SupplyFilterBuilder;
import com.ubt.stock.managesupplies.models.SupplyModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AlertNotificationsServiceImpl implements AlertNotificationsService {
  private final int ONE_MINUTE_IN_MILLIS = 1000 * 60;
  private final int ONE_HOUR_IN_MILLIS = ONE_MINUTE_IN_MILLIS * 60;
  private final AlertConditionsService alertConditionsService;
  private final ManageSuppliesService manageSuppliesService;
  private final NotificationsFeignClient notificationsFeignClient;
  private final PosFeignClient posFeignClient;

  @Autowired
  public AlertNotificationsServiceImpl(AlertConditionsService alertConditionsService, ManageSuppliesService manageSuppliesService, NotificationsFeignClient notificationsFeignClient, PosFeignClient posFeignClient) {
    this.alertConditionsService = alertConditionsService;
    this.manageSuppliesService = manageSuppliesService;
    this.notificationsFeignClient = notificationsFeignClient;
    this.posFeignClient = posFeignClient;
  }

  /**
   * Creates alerts based on alert conditions that are added by admin.
   * This method is scheduled to run every 1H.
   */
  @Override
  @Scheduled(fixedDelay = ONE_HOUR_IN_MILLIS)
  public void processAlerts() {
    List<AlertConditionModel> alertConditions = alertConditionsService.getAlertConditions(null);
    List<NotificationModel> notificationsToSend = new ArrayList<>();

    List<Long> notifiedAlertIds = new ArrayList<>();

    for (AlertConditionModel alertCondition : alertConditions) {
      //if it is already notified continue with the next one
      if (Boolean.TRUE.equals(alertCondition.getNotified())) {
        continue;
      }

      Long productId = alertCondition.getProductId();
      Long branchId = alertCondition.getPharmacyLocationId();
      Integer quantity = alertCondition.getQuantity();

      NotificationDataModel notificationDataModel = new NotificationDataModel();
      notificationDataModel.setProductId(alertCondition.getProductId());
      notificationDataModel.setValue(alertCondition.getQuantity());

      List<SaleModel> sales = posFeignClient.getSales(List.of(productId), branchId);

      if (AlertTypes.STOCK_DROPS_TO.equals(alertCondition.getAlertConditionTypeId())) {
        if (stockDropTo(quantity, productId, branchId, sales)) {
          notifiedAlertIds.add(alertCondition.getId());

          notificationsToSend.add(
              NotificationModelBuilder.aNotificationModel()
                  .withUserId(alertCondition.getPharmacyLocationId())
                  .withTypeId(AlertTypes.STOCK_DROPS_TO)
                  .withData(notificationDataModel)
                  .build()
          );

          alertConditionsService.updateAlertNotifiedStatus(null, alertCondition.getId(), null, true);
        }
      } else if (AlertTypes.WEEKLY_SALES_OF.equals(alertCondition.getAlertConditionTypeId())) {
        if (weeklySalesOf(quantity, sales)) {
          notifiedAlertIds.add(alertCondition.getId());

          notificationsToSend.add(
              NotificationModelBuilder.aNotificationModel()
                  .withUserId(alertCondition.getPharmacyLocationId())
                  .withTypeId(AlertTypes.WEEKLY_SALES_OF)
                  .withData(notificationDataModel)
                  .build()
          );

          alertConditionsService.updateAlertNotifiedStatus(null, alertCondition.getId(), null, true);
        }
      }
    }

    notificationsFeignClient.addAll(notificationsToSend);
  }

  private boolean stockDropTo(Integer value, Long productId, Long branchId, List<SaleModel> sales) {
    List<SupplyModel> supplies = manageSuppliesService.getSupplies(
        SupplyFilterBuilder.aSupplyFilter()
            .withProductId(productId)
            .withBranchId(branchId)
            .build(),
        Pageable.unpaged()
    ).getContent();

    Optional<Integer> suppliesQuantity = supplies.stream().map(SupplyModel::getQuantity).reduce(Integer::sum);
    Optional<Integer> salesQuantity = sales.stream().map(SaleModel::getQuantity).reduce(Integer::sum);

    if (suppliesQuantity.isPresent() && salesQuantity.isPresent()) {
      return suppliesQuantity.get() - salesQuantity.get() <= value;
    }

    return false;
  }

  private boolean weeklySalesOf(Integer value, List<SaleModel> sales) {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.MONTH, -1);

    List<SaleModel> salesOfMonth = sales.stream().filter(saleModel -> saleModel.getCreatedDate() <= calendar.getTimeInMillis()).collect(Collectors.toList());

    Optional<Integer> quantity = salesOfMonth.stream().map(SaleModel::getQuantity).reduce(Integer::sum);

    return quantity.filter(integer -> integer >= value).isPresent();
  }
}
