/*
 * This file is generated by jOOQ.
 */
package com.ubt.stock.jooq.tables.records;


import com.ubt.stock.jooq.tables.AlertConditionTypes;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class AlertConditionTypesRecord extends UpdatableRecordImpl<AlertConditionTypesRecord> implements Record2<Long, String> {

    private static final long serialVersionUID = -1540869610;

    /**
     * Setter for <code>public.alert_condition_types.id</code>.
     */
    public void setId(Long value) {
        set(0, value);
    }

    /**
     * Getter for <code>public.alert_condition_types.id</code>.
     */
    public Long getId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>public.alert_condition_types.description</code>.
     */
    public void setDescription(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>public.alert_condition_types.description</code>.
     */
    public String getDescription() {
        return (String) get(1);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record2 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row2<Long, String> fieldsRow() {
        return (Row2) super.fieldsRow();
    }

    @Override
    public Row2<Long, String> valuesRow() {
        return (Row2) super.valuesRow();
    }

    @Override
    public Field<Long> field1() {
        return AlertConditionTypes.ALERT_CONDITION_TYPES.ID;
    }

    @Override
    public Field<String> field2() {
        return AlertConditionTypes.ALERT_CONDITION_TYPES.DESCRIPTION;
    }

    @Override
    public Long component1() {
        return getId();
    }

    @Override
    public String component2() {
        return getDescription();
    }

    @Override
    public Long value1() {
        return getId();
    }

    @Override
    public String value2() {
        return getDescription();
    }

    @Override
    public AlertConditionTypesRecord value1(Long value) {
        setId(value);
        return this;
    }

    @Override
    public AlertConditionTypesRecord value2(String value) {
        setDescription(value);
        return this;
    }

    @Override
    public AlertConditionTypesRecord values(Long value1, String value2) {
        value1(value1);
        value2(value2);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached AlertConditionTypesRecord
     */
    public AlertConditionTypesRecord() {
        super(AlertConditionTypes.ALERT_CONDITION_TYPES);
    }

    /**
     * Create a detached, initialised AlertConditionTypesRecord
     */
    public AlertConditionTypesRecord(Long id, String description) {
        super(AlertConditionTypes.ALERT_CONDITION_TYPES);

        set(0, id);
        set(1, description);
    }
}
