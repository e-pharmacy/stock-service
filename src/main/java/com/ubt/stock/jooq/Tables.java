/*
 * This file is generated by jOOQ.
 */
package com.ubt.stock.jooq;


import com.ubt.stock.jooq.tables.AlertConditionTypes;
import com.ubt.stock.jooq.tables.AlertConditions;
import com.ubt.stock.jooq.tables.Alerts;
import com.ubt.stock.jooq.tables.Branches;
import com.ubt.stock.jooq.tables.Supplies;


/**
 * Convenience access to all tables in public
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tables {

    /**
     * The table <code>public.alert_condition_types</code>.
     */
    public static final AlertConditionTypes ALERT_CONDITION_TYPES = AlertConditionTypes.ALERT_CONDITION_TYPES;

    /**
     * The table <code>public.alert_conditions</code>.
     */
    public static final AlertConditions ALERT_CONDITIONS = AlertConditions.ALERT_CONDITIONS;

    /**
     * The table <code>public.alerts</code>.
     */
    public static final Alerts ALERTS = Alerts.ALERTS;

    /**
     * The table <code>public.branches</code>.
     */
    public static final Branches BRANCHES = Branches.BRANCHES;

    /**
     * The table <code>public.supplies</code>.
     */
    public static final Supplies SUPPLIES = Supplies.SUPPLIES;
}
