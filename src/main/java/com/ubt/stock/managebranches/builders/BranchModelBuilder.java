/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 27/07/2021.
 */

package com.ubt.stock.managebranches.builders;

import com.ubt.stock.managebranches.models.BranchModel;

public final class BranchModelBuilder {
  private Long id;
  private Long createdDate;
  private Long updatedDate;
  private String address;

  private BranchModelBuilder() {
  }

  public static BranchModelBuilder aBranchModel() {
    return new BranchModelBuilder();
  }

  public BranchModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public BranchModelBuilder withCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
    return this;
  }

  public BranchModelBuilder withUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
    return this;
  }

  public BranchModelBuilder withAddress(String address) {
    this.address = address;
    return this;
  }

  public BranchModel build() {
    BranchModel branchModel = new BranchModel();
    branchModel.setId(id);
    branchModel.setCreatedDate(createdDate);
    branchModel.setUpdatedDate(updatedDate);
    branchModel.setAddress(address);
    return branchModel;
  }
}
