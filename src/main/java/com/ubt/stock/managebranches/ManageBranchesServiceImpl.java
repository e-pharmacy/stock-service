package com.ubt.stock.managebranches;

import com.ubt.stock.managebranches.models.BranchModel;
import com.ubt.stock.managebranches.modelsvalidations.BranchModelValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ManageBranchesServiceImpl implements ManageBranchesService {
  private final ManageBranchesRepository manageBranchesRepository;

  @Autowired
  public ManageBranchesServiceImpl(ManageBranchesRepository manageBranchesRepository) {
    this.manageBranchesRepository = manageBranchesRepository;
  }

  @Override
  public BranchModel createBranch(BranchModel branchModel) {
    BranchModelValidator.isValidOnCreate(branchModel);
    return manageBranchesRepository.createBranch(branchModel);
  }

  @Override
  public List<BranchModel> getBranches(List<Long> ids, String name) {
    return manageBranchesRepository.getBranches(ids, name);
  }

  @Override
  public BranchModel getBranchById(Long id) {
    return manageBranchesRepository.getBranchById(id);
  }

  @Override
  public BranchModel updateBranch(Long id, BranchModel branchModel) {
    BranchModelValidator.isValidOnCreate(branchModel);
    return manageBranchesRepository.updateBranch(id, branchModel);
  }

  @Override
  public BranchModel deleteBranch(Long id) {
    return manageBranchesRepository.deleteBranch(id);
  }
}
