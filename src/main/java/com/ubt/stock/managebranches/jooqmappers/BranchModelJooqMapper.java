/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 27/07/2021.
 */

package com.ubt.stock.managebranches.jooqmappers;

import com.ubt.stock.jooq.tables.records.BranchesRecord;
import com.ubt.stock.managebranches.builders.BranchModelBuilder;
import com.ubt.stock.managebranches.models.BranchModel;

import java.util.ArrayList;
import java.util.List;

public class BranchModelJooqMapper {
  public static BranchModel map(BranchesRecord branchesRecord) {
    return BranchModelBuilder.aBranchModel()
        .withId(branchesRecord.getId())
        .withAddress(branchesRecord.getAddress())
        .build();
  }

  public static BranchesRecord unmap(BranchModel branchModel) {
    BranchesRecord branchesRecord = new BranchesRecord();

    if (branchModel.getId() != null) {
      branchesRecord.setId(branchModel.getId());
    }

    if (branchModel.getAddress() != null) {
      branchesRecord.setAddress(branchModel.getAddress());
    }

    return branchesRecord;
  }

  public static List<BranchModel> map(List<BranchesRecord> branchesRecords) {
    List<BranchModel> branches = new ArrayList<>();

    for (BranchesRecord branchesRecord : branchesRecords) {
      branches.add(map(branchesRecord));
    }

    return branches;
  }
}
