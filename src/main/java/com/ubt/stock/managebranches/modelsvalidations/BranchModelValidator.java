/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 27/07/2021.
 */

package com.ubt.stock.managebranches.modelsvalidations;

import com.ubt.stock.exceptions.BadRequestException;
import com.ubt.stock.managebranches.models.BranchModel;

public class BranchModelValidator {
  public static void isValidOnCreate(BranchModel branchModel) {
    isObjectEmpty(branchModel);
  }

  public static void isValidOnUpdate(Long id, BranchModel branchModel) {
    isObjectEmpty(branchModel);

    if (branchModel.getId() == null || branchModel.getId() == 0) {
      throw new BadRequestException("Invalid branch id!");
    }
  }

  /**
   * Throws an exception if any required value of the object is not provided.
   *
   * @param branchModel
   * @throws BadRequestException
   */
  private static void isObjectEmpty(BranchModel branchModel) {
    if (branchModel.getAddress() == null || branchModel.getAddress().trim().isEmpty()) {
      throw new BadRequestException("Invalid branch address name!");
    }
  }
}
