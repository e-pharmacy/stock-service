package com.ubt.stock.managebranches;


import com.ubt.stock.managebranches.models.BranchModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/branches")
public class ManageBranchesController {
  private final ManageBranchesService manageBranchesService;

  @Autowired
  public ManageBranchesController(ManageBranchesService manageBranchesService) {
    this.manageBranchesService = manageBranchesService;
  }

  @PostMapping("")
  public BranchModel createBranch(@RequestBody BranchModel branchModel) {
    return manageBranchesService.createBranch(branchModel);
  }

  @GetMapping("")
  public List<BranchModel> getBranches(@RequestParam(required = false) List<Long> ids, @RequestParam(required = false) String name) {
    //TODO Add ModelFilter and put "name" field on it.
    return manageBranchesService.getBranches(ids, name);
  }

  @GetMapping("/{id}")
  public BranchModel getBranchById(@PathVariable Long id) {
    return manageBranchesService.getBranchById(id);
  }

  @PutMapping("/{id}")
  public BranchModel updateBranch(@PathVariable Long id, @RequestBody BranchModel branchModel) {
    return manageBranchesService.updateBranch(id, branchModel);
  }

  @DeleteMapping("/{id}")
  public BranchModel deleteBranch(@PathVariable Long id) {
    return manageBranchesService.deleteBranch(id);
  }
}
