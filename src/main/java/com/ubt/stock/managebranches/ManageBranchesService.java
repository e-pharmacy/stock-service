package com.ubt.stock.managebranches;

import com.ubt.stock.managebranches.models.BranchModel;

import java.util.List;

public interface ManageBranchesService {
  BranchModel createBranch(BranchModel branchModel);

  List<BranchModel> getBranches(List<Long> ids, String name);

  BranchModel getBranchById(Long id);

  BranchModel updateBranch(Long id, BranchModel branchModel);

  BranchModel deleteBranch(Long id);
}
