package com.ubt.stock.managebranches;

import com.ubt.stock.jooq.tables.records.BranchesRecord;
import com.ubt.stock.managebranches.jooqmappers.BranchModelJooqMapper;
import com.ubt.stock.managebranches.models.BranchModel;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.ubt.stock.jooq.Tables.BRANCHES;

@Repository
public class ManageBranchesRepositoryImpl implements ManageBranchesRepository {
  private final DSLContext create;

  @Autowired
  public ManageBranchesRepositoryImpl(DSLContext create) {
    this.create = create;
  }

  @Override
  public BranchModel createBranch(BranchModel branchModel) {
    BranchesRecord branchesRecord = BranchModelJooqMapper.unmap(branchModel);

    BranchesRecord insertedBranchRecord = create.insertInto(BRANCHES)
        .set(branchesRecord)
        .returning()
        .fetchOne();
    return BranchModelJooqMapper.map(insertedBranchRecord);
  }

  @Override
  public List<BranchModel> getBranches(List<Long> ids, String name) {
    Condition condition = DSL.trueCondition();
    if (ids != null && !ids.isEmpty()) {
      condition = BRANCHES.ID.in(ids);
    }

    if (name != null && !name.isEmpty()) {
      condition = BRANCHES.ADDRESS.containsIgnoreCase(name);
    }

    List<BranchesRecord> branchesRecordList = create.selectFrom(BRANCHES)
        .where(condition)
        .fetch();

    return BranchModelJooqMapper.map(branchesRecordList);
  }

  @Override
  public BranchModel getBranchById(Long id) {
    BranchesRecord branchesRecord = create.selectFrom(BRANCHES)
        .where(BRANCHES.ID.eq(id))
        .fetchOne();

    return BranchModelJooqMapper.map(branchesRecord);
  }

  @Override
  public BranchModel updateBranch(Long id, BranchModel branchModel) {
    BranchesRecord branchesRecord = BranchModelJooqMapper.unmap(branchModel);

    BranchesRecord updatedBranchesRecord = create.update(BRANCHES)
        .set(branchesRecord)
        .where(BRANCHES.ID.eq(id))
        .returning()
        .fetchOne();

    return BranchModelJooqMapper.map(updatedBranchesRecord);
  }

  @Override
  public BranchModel deleteBranch(Long id) {
    BranchesRecord deletedBranchesRecord = create.deleteFrom(BRANCHES)
        .where(BRANCHES.ID.eq(id))
        .returning()
        .fetchOne();

    return BranchModelJooqMapper.map(deletedBranchesRecord);
  }
}
